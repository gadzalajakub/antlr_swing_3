grammar Expr;

options {
  output=AST;
  ASTLabelType=CommonTree;
}

@header {
package tb.antlr;
}

@lexer::header {
package tb.antlr;
}

prog
    : (stat | blok)+ EOF!
    ;
    
blok
    : BEGIN^ NL! (stat | blok)* END!
    ;

stat
    : expr NL -> expr
    | VAR ID (ASSIGN expr)? NL -> ^(VAR ID) ^(ASSIGN ID expr)?
    | ID ASSIGN expr NL -> ^(ASSIGN ID expr)  
    | ifStat NL -> ifStat
    | NL ->
    ;

expr
    : multExpr
      ( PLUS^ multExpr
      | MINUS^ multExpr
      )*
    ;

ifExpr
  : expr
  (EQ^ expr
  |NEQ^ expr
  )*
  ;

multExpr
    : powExpr
      ( MUL^ atom
      | DIV^ atom
      )*
    ;
powExpr
  : atom
  (POW^powExpr)?
  ;
atom
    : INT
    | ID
    | LP! expr RP!
    ;

ifStat
  : IF^ ifExpr THEN! NL? blok (ELSE! NL? blok)?
  ;
  
VAR 
  :'var'
  ;
  
IF 
  : 'if'
  ;

ELSE
  : 'else'
  ;
 
THEN
  : 'then'
  ;
BEGIN
  : '{'
  ;

END
  : '}'
  ;

EQ
  : '=='
  ;

NEQ
  : '!='
  ;
  
ID : ('a'..'z'|'A'..'Z'|'_') ('a'..'z'|'A'..'Z'|'0'..'9'|'_')*;

INT : '0'..'9'+;

NL : '\r'? '\n' ;

WS : (' ' | '\t')+ {$channel = HIDDEN;} ;


LP
	:	'('
	;

RP
	:	')'
	;

ASSIGN
  : '='
  ;

PLUS
	:	'+'
	;

MINUS
	:	'-'
	;

MUL
	:	'*'
	;

DIV
	:	'/'
	;
POW
  : '^'
  ;
