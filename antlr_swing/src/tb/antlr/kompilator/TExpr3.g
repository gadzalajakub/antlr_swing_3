tree grammar TExpr3;

options {
  tokenVocab=Expr;

  ASTLabelType=CommonTree;

  output=template;
  superClass = TreeParserTmpl;
}

@header {
package tb.antlr.kompilator;
}

@members {
  Integer numer = 0;
  Integer count = 0;
}
prog    : (e+=zakres | e+=expr | e+=comp | d+=decl)* -> program(name={$e}, declarations={$d})
;

zakres  : ^(BEGIN {enterScope();} (e+=zakres | e+=expr | e+=comp | d+=decl)* {leaveScope();}) -> blok(statement={$e}, declaration={$d})
;


decl  :
        ^(VAR i1=ID) {locals.newSymbol($ID.text);} -> declare(p1={$ID.text})
    ;
    catch [RuntimeException ex] {errorID(ex,$i1);}

expr    : ^(PLUS  e1=expr e2=expr) -> plus(p1={$e1.st},p2={$e2.st})
        | ^(MINUS e1=expr e2=expr) -> minus(p1={$e1.st},p2={$e2.st})
        | ^(MUL   e1=expr e2=expr) -> mul(p1={$e1.st},p2={$e2.st})
        | ^(DIV   e1=expr e2=expr) -> div(p1={$e1.st},p2={$e2.st})
        | ^(ASSIGN i1=ID e2=expr) {locals.hasSymbol($i1.text);} -> assign(p1={$ID}, p2={$e2.st})
        | INT                      -> int(i={$INT.text},j={numer.toString()})
        | ID                       {locals.checkSymbol($ID.text);}-> id(i={$ID.text})
        | ^(IF if_processed=comp then_block=zakres else_block=zakres?) {count++;} -> if(if_processed={$if_processed.st}, then_block={$then_block.st}, else_block={$else_block.st}, nr={count.toString()})
    ;
    
comp    : ^(EQ e1=expr e2=expr) -> isEqual(p1={$e1.st}, p2={$e2.st})
        | ^(NEQ e1=expr e2=expr) ->isNotEqual(p1={$e1.st},p2={$e2.st}, nr={count.toString()})
        ;
    